/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package unocardgame;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Number extends MainCard {
    private  int number;

    public Number(String symbol,MainColour color ,int  number) {
        super(symbol, color);
        this. number = number;
        score = number;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public boolean isValidCard(MainCard currentCard) {
        return (currentCard instanceof Number && this.number == ((Number) currentCard).getNumber())
                || this.getColor() == currentCard.getColor();
    }
}
