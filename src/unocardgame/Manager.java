/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package unocardgame;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Manager {
    private ArrayList<MainPlayer> players;
    private ArrayList<MainCard> cards;
    private ArrayList<MainControl> controls;
    private MainState s;
    private MainCard currentCard;
    private boolean gameStatus;

    public Manager(int numberOfPlayers, int numberOfPcPlayers, String[] names, Scanner scanner) {
        gameStatus = true;
        s= new MainState(numberOfPlayers);
        cards = new ArrayList<>();
        for (int i = 0; i <= (numberOfPlayers-1)/10; i++) {
            makeTheCards();
        }
        players = new ArrayList<>();
        controls = new ArrayList<>();
        for (int i = 0; i < numberOfPlayers; i++) {
            ArrayList<MainCard> playerCards = new ArrayList<>();
            for (int j = 0; j < 7; j++) {
                playerCards.add(cards.get(0));
                cards.remove(0);
            }
            players.add(new MainPlayer(names[i], playerCards));
            if (i < numberOfPcPlayers) {
                controls.add(new ComputerControl(players.get(i),s));
            }
            else {
                controls.add((MainControl) new PlayerControl(players.get(i), scanner));
            }
        }
        putTheCurrentCard();
    }

    private void makeTheCards() {
        for (int i = 0; i < 10; i++) {
            String s = i + "";
            cards.add(new Number(s, MainColour.RED, i));
            cards.add(new Number(s, MainColour.GREEN, i));
            cards.add(new Number(s, MainColour.BLUE, i));
            cards.add(new Number(s, MainColour.YELLOW, i));
            if(i > 0) {
                cards.add(new Number(s, MainColour.RED, i));
                cards.add(new Number(s, MainColour.GREEN, i));
                cards.add(new Number(s, MainColour.BLUE, i));
                cards.add(new Number(s, MainColour.YELLOW, i));
            }
        }

        for (int i = 0; i < 4; i++) {
            cards.add(new Wild("W", MainColour.BLACK));
            cards.add(new WildDraw("W+4", MainColour.BLACK));
        }

        for (int i = 0; i < 2; i++) {
            cards.add(new Reverse("Rev", MainColour.RED));
            cards.add(new Reverse("Rev", MainColour.GREEN));
            cards.add(new Reverse("Rev", MainColour.BLUE));
            cards.add(new Reverse("Rev", MainColour.YELLOW));
        }

        for (int i = 0; i < 2; i++) {
            cards.add(new Draw2("D+2", MainColour.RED));
            cards.add(new Draw2("D+2", MainColour.GREEN));
            cards.add(new Draw2("D+2", MainColour.BLUE));
            cards.add(new Draw2("D+2", MainColour.YELLOW));
        }

        for (int i = 0; i < 2; i++) {
            cards.add(new Skip("Ski", MainColour.RED));
            cards.add(new Skip("Ski", MainColour.GREEN));
            cards.add(new Skip("Ski", MainColour.BLUE));
            cards.add(new Skip("Ski", MainColour.YELLOW));
        }
        Collections.shuffle(cards);
    }
    
    private void putTheCurrentCard() {
        int temp = 0;
        while (true) {
            if(!(cards.get(temp) instanceof Wild) ) {
                currentCard = cards.get(temp);
                cards.remove(temp);
                break;
            }
            temp++;
        }
        if (!(currentCard instanceof Number)) {
            print();
        }
        if (currentCard instanceof Reverse) {
            ((Reverse) currentCard).alterTheState(s);
        }
        if (currentCard instanceof Skip) {
            ((Skip) currentCard).alterTheState(s);
        }
        if (currentCard instanceof Draw2) {
            System.out.println("The player was fined.");
            for (int i = 0; i < ((Draw2) currentCard).getForcedCards(); i++) {
                players.get(s.getTurn()).addCard(cards.get(0));
                cards.remove(0);
            }
            s.nextTurn();
        }
    }

    private void alterTheCurrentCard(MainCard newCurCard) {
        if (currentCard instanceof Wild) {
            currentCard.setColor(MainColour.BLACK);
        }
        cards.add(new Random().nextInt(cards.size()) ,this.currentCard);
        this.currentCard = newCurCard;
    }

    private void setGameStatus() {
        this.gameStatus = false;
    }

    private void checkFinish() {
        for (MainPlayer temp: players) {
            if (temp.getNumberOfCards() == 0) {
                System.out.println("...................................................................................................................................................................");
                System.out.println("Game Over.\n");
                System.out.println(MainColour.PURPLE.getPrintColor() + temp.getName() + " is the winner.\n" + MainColour.RESET.getPrintColor());
                setGameStatus();
            }
        }
        if (!gameStatus) {
            System.out.println(MainColour.RESET.getPrintColor() + "Results:\n");
            for (MainPlayer temp: players) {
                if (temp.getNumberOfCards() != 0) {
                    System.out.println(MainColour.CYAN.getPrintColor() + temp.toString());
                    System.out.print(temp.handToString() + MainColour.RESET.getPrintColor());
                }
            }
        }
    }

    private void print() {
        System.out.println("................................................................................................................................................");
        System.out.print(MainColour.RESET.getPrintColor() + "║");
        for(int i = 0; i < players.size(); i++) {
            if (s.getTurn() == i) {
                System.out.print(MainColour.PURPLE.getPrintColor());
                System.out.print((i+1) + "- " + players.get(i).toString());
                System.out.print(MainColour.RESET.getPrintColor() + "║");
            }
            else {
                System.out.print(MainColour.CYAN.getPrintColor());
                System.out.print((i+1) + "- " + players.get(i).toString());
                System.out.print(MainColour.RESET.getPrintColor() + "║");
            }
        }
        System.out.println();
        System.out.print("Direction: ");
        if (s.getDirection() == +1) {
            System.out.println(MainColour.PURPLE.getPrintColor() + "clockwise ↻");
        }
        else {
            System.out.println(MainColour.PURPLE.getPrintColor() + "anticlockwise ↺");
        }
        System.out.println(MainColour.RESET.getPrintColor() + "Turn: " + MainColour.PURPLE.getPrintColor() + players.get(s.getTurn()).getName() + MainColour.RESET.getPrintColor());
        System.out.println(currentCard.currToString());
        if (controls.get(s.getTurn()) instanceof ComputerControl) {
            System.out.print(players.get(s.getTurn()).backHandToString() + MainColour.RESET.getPrintColor());
        }
        else {
            System.out.print(players.get(s.getTurn()).handToString() + MainColour.RESET.getPrintColor());
        }
    }

    public void playTheGame() {
        print();
        while (gameStatus) {
            if (canPut(players.get((s.getTurn())))) {
                int choice = -1;
                while (gameStatus) {
                    choice = controls.get(s.getTurn()).putCard(currentCard);
                    if (validInput(players.get((s.getTurn())), choice)) {
                        System.out.println(players.get(s.getTurn()).getCards().get(choice).toString());
                        try{Thread.sleep(1000);}catch(InterruptedException ignored){}
                        break;
                    }
                }
                decide(players.get(s.getTurn()), players.get(s.getTurn()).getCards().get(choice));
            }
            try{Thread.sleep(500);}catch(InterruptedException ignored){}
            checkFinish();
            if (gameStatus) {
                s.nextTurn();
                print();
            }
        }
    }

    private void decide(MainPlayer player, MainCard choiceCard) {
        player.removeCard(choiceCard);
        if (choiceCard instanceof WildDraw) {
            while (true) {
                char color = controls.get(s.getTurn()).selectTheColor();
                if (color == 'R' || color == 'G' || color == 'Y' || color == 'B') {
                    ((WildDraw) choiceCard).setConfig(color);
                    break;
                }
                System.out.println("Invalid color input.");
            }
            char ch = 0;
            if(canChooseWildDrawCardPenalty(players.get(s.getNextTurn()))) {
                while (true) {
                    ch = controls.get(s.getNextTurn()).yesOrNo();
                    if (ch == 'Y' || ch == 'N'){
                        break;
                    }
                    System.out.println("Invalid input.");
                }
            }
            if (ch == 'N') {
                s.increaseWildDrawPenalty();
            }
            else {
                ((WildDraw) choiceCard).alterTheState(s);
                for (int i = 0; i < ((WildDraw) choiceCard).getForcedCards()*s.getWildDrawPenalty(); i++) {
                    players.get(s.getTurn()).addCard(cards.get(0));
                    cards.remove(0);
                }
                s.doWildDrawPenalty();
            }
        }

        else if (choiceCard instanceof Wild) {
            while (true) {
                char color = controls.get(s.getTurn()).selectTheColor();
                if (color == 'R' || color == 'G' || color == 'Y' || color == 'B') {
                    ((Wild) choiceCard).setConfig(color);
                    break;
                }
                System.out.println("Invalid color input.");
            }
        }

        if (choiceCard instanceof Reverse) {
            ((Reverse) choiceCard).alterTheState(s);
        }

        if (choiceCard instanceof Skip) {
            ((Skip) choiceCard).alterTheState(s);
        }

        if (choiceCard instanceof Draw2) {
            char ch = 0;
            if(canChooseDrawCardPenalty(players.get(s.getNextTurn()))) {
                while (true) {
                    ch = controls.get(s.getNextTurn()).yesOrNo();
                    if (ch == 'Y' || ch == 'N'){
                        break;
                    }
                    System.out.println("Invalid input.");
                }
            }
            if (ch == 'N') {
                s.increaseDrawPenalty();
            }
            else {
                ((Draw2) choiceCard).alterTheState(s);
                for (int i = 0; i < ((Draw2) choiceCard).getForcedCards()*s.getDrawPenalty(); i++) {
                    players.get(s.getTurn()).addCard(cards.get(0));
                    cards.remove(0);
                }
                s.doDrawPenalty();
            }
        }
        alterTheCurrentCard(choiceCard);
    }

    private boolean canPut(MainPlayer player) {
        boolean haveValidCart = false;
        boolean can = false;
        for (MainCard temp: player.getCards()) {
            if(temp.isValidCard(currentCard) || temp instanceof Wild) {
                haveValidCart = true;
                can = true;
                break;
            }
        }
        if (!haveValidCart) {
            player.addCard(cards.get(0));
            cards.remove(0);
            if (player.getCards().get(player.getCards().size()-1).isValidCard(currentCard)) {
                print();
                System.out.println(player.getName() + "! You were given a card because you didn't have any valid choice.");
                can = true;
            }
            else  {
                print();
                System.out.println(player.getName() + "! You were given a card but you don't have any valid choice yet.");
            }
        }
        return can;
    }

    private boolean validInput(MainPlayer player, int choice) {
        if (0 <= choice && choice < player.getCards().size()) {
            if(s.getWildDrawPenalty() != 1 && player.getCards().get(choice) instanceof WildDraw) {
                return true;
            }
            if( s.getWildDrawPenalty() != 1 && !(player.getCards().get(choice) instanceof WildDraw)) {
                System.out.println("You should put WildDraw card.");
                return false;
            }
            if( player.getCards().get(choice) instanceof WildDraw){
                boolean validWildDraw = true;
                for (MainCard temp: player.getCards()) {
                    if(temp.isValidCard(currentCard)) {
                        validWildDraw = false;
                    }
                }
                if (validWildDraw) {
                    return true;
                }
                else {
                    System.out.println("You can't put WildDraw card.");
                    return false;
                }
            }
            if(s.getDrawPenalty() != 1 && player.getCards().get(choice) instanceof  Draw2) {
                return true;
            }
            if (s.getDrawPenalty() != 1 && !(player.getCards().get(choice) instanceof  Draw2)) {
                System.out.println("You should put Draw2 card.");
                return false;
            }
            if ( player.getCards().get(choice).isValidCard(currentCard)) {
                return true;
            }
            System.out.println("Enter valid card.");
            return false;
        }
        System.out.println("Enter a number between 1-" + player.getCards().size() );
        return false;
    }

    private boolean canChooseDrawCardPenalty(MainPlayer nextPlayer) {
        boolean can = false;
        for (MainCard temp : nextPlayer.getCards()) {
            if (temp instanceof Draw2) {
                can = true;
                break;
            }
        }
        if (!can) {
            System.out.println("The next player was fined.");
        }
        return can;
    }

    private boolean canChooseWildDrawCardPenalty(MainPlayer nextPlayer) {
        boolean can = false;
        for (MainCard temp : nextPlayer.getCards()) {
            if (temp instanceof WildDraw) {
                can = true;
                break;
            }
        }
        if (!can) {
            System.out.println("The next player was fined.");
        }
        return can;
    }
}
