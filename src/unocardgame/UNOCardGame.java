/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package unocardgame;
import java.util.Scanner;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class UNOCardGame {
     public static void main(String[] args) {
        // TODO code application logic here
        Scanner scanner = new Scanner(System.in);
        int players;
        int compPlayers = 0;
        while (true) {
            System.out.println("Enter mode:");
            System.out.println("3 & 3 Players (2 Computer)");
            System.out.println("4 & 4 Players (3 Computer)");
            System.out.println("5 & 5 Players (4 Computer)");
            System.out.println("6 & n Players");
              players = scanner.next().trim().charAt(0)-'0';
            if ( 3 <= players && players <= 6) {
                if(players == 3) {
                    compPlayers = 2;
                }
                if(players == 4) {
                    compPlayers = 3;
                }
                if(players == 5) {
                    compPlayers = 4;
                }
                if (players == 6){
                    while (true) {
                        System.out.println("Please enter how many players do you want? (2-n)");
                        try {
                            players = scanner.nextInt();
                        } catch (Exception InputMismatchException) {
                            scanner.next();
                            players = -1;
                        }
                        if( 2 <= players) {
                            break;
                        }
                        System.out.println("Enter a number between 2-n");
                    }
                    while (true) {
                        System.out.println("How many PCComputer players do you want? (0 to" + players + ")");
                        try {
                            compPlayers = scanner.nextInt();
                        } catch (Exception InputMismatchException) {
                            scanner.next();
                            compPlayers = -1;
                        }
                        if( 0 <= compPlayers && compPlayers <= players) {
                            break;
                        }
                        System.out.println("Please enter a number between 0 to"+ players);
                    }
                }
                break;
            }
            System.out.println("Please enter a number between 3 to 6");
        }
        String[] names = new String[players];
        if (compPlayers > 0) {
            System.out.println("Enter name of Computer players.");
        }
        for(int i =0; i < compPlayers; i++) {
            System.out.print( (i+1) + "th Computer player name: ");
            names[i] = scanner.next().trim() + "(PC)";
        }
        if ((players - compPlayers) > 0) {
            System.out.println("Enter name of the Human players.");
        }
        for(int i = 0; i < (players - compPlayers); i++) {
            System.out.print( (i+1) + "th Human player name: ");
            names[compPlayers+i] = scanner.next().trim();
        }
        Manager UNO = new Manager(players, compPlayers, names, scanner);
        UNO.playTheGame();
    }    
}
