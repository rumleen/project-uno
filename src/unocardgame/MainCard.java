/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package unocardgame;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class MainCard {
    private String sym;
    private MainColour color;
    protected int score;

    public MainCard(String sym, MainColour color)
    {
        this.sym = sym;
        this.color = color;
    }

    public boolean isValidCard(MainCard curCard) {
        return true;
    }

    public String getSymbol() {
        return sym;
    }

    public MainColour getColor()
    {
        return color;
    }

    public int getScore() {
        return score;
    }

    public void setColor(MainColour color) {
        this.color = color;
    }

    @Override
    public String toString()
    {
        StringBuilder tCard = new StringBuilder();
        String printColor = color.getPrintColor();
        tCard.append(printColor);
        tCard.append("╔═══════╗\n");
        tCard.append(printColor);
        tCard.append("║       ║\n");
        tCard.append(printColor);
        if (sym.equals("W+4")||sym.equals("Rev")||sym.equals("D+2")||sym.equals("Ski")) {
            tCard.append("║  ").append(sym).append("  ║\n");
        }
        else {
            tCard.append("║   ").append(sym).append("   ║\n");
        }
        tCard.append(printColor);
        tCard.append("║       ║\n");
        tCard.append(printColor);
        tCard.append("╚═══════╝");
        tCard.append(MainColour.RESET.getPrintColor());
        return tCard.toString();
    }

    public String currToString()
    {
        StringBuilder tCard = new StringBuilder();
        String printColor = color.getPrintColor();
        tCard.append(MainColour.BLACK.getPrintColor());
        tCard.append("                  ╔═══════╗ ");
        tCard.append(printColor);
        tCard.append("╔═══════╗\n");
        tCard.append(MainColour.BLACK.getPrintColor());
        tCard.append("                  ║#######║ ");
        tCard.append(printColor);
        tCard.append("║       ║\n");
        tCard.append(MainColour.BLACK.getPrintColor());
        tCard.append("                  ║#######║ ");
        if (sym.equals("W+4")||sym.equals("Rev")||sym.equals("D+2")||sym.equals("Ski")) {
            tCard.append(printColor).append("║  ").append(sym).append("  ║\n");
        }
        else {
            tCard.append(printColor).append("║   ").append(sym).append("   ║\n");
        }
        tCard.append(MainColour.BLACK.getPrintColor());
        tCard.append("                  ║#######║ ");
        tCard.append(printColor);
        tCard.append("║       ║\n");
        tCard.append(MainColour.BLACK.getPrintColor());
        tCard.append("                  ╚═══════╝ ");
        tCard.append(printColor);
        tCard.append("╚═══════╝\n");
        tCard.append(MainColour.BLACK.getPrintColor());
        return tCard.toString();
    }

    public String backToTheString()
    {
        return MainColour.BLACK.getPrintColor() +
                "╔═══════╗\n" +
                "║#######║\n" +
                "║#######║\n" +
                "║#######║\n" +
                "╚═══════╝" +
                MainColour.RESET.getPrintColor();
    }
}
