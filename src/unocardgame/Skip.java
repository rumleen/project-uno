/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package unocardgame;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Skip extends ActionCard {
    public Skip(String symbol,MainColour color) {
        super(symbol, color);
    }

    @Override
    public boolean isValidCard(MainCard currentCard) {
        return currentCard.getColor() == this.getColor() || currentCard instanceof Skip;
    }

    @Override
    public void alterTheState(MainState s) {
        s.nextTurn();
    }
}
