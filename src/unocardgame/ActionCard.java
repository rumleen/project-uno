/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package unocardgame;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class ActionCard extends MainCard {
    public ActionCard(String symbol, MainColour color) {
            super(symbol, color);
            score = 20;
        }

        public void alterTheState(MainState s) {
        }

        public int getForcedCards(){
            return 0;
        }
    
}
