/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package unocardgame;
import java.util.ArrayList;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class MainPlayer {
     private String name;
    private ArrayList<MainCard> hand;
    private int score;

    public MainPlayer(String name, ArrayList<MainCard> hand) {
        this.name = name;
        this.hand = hand;
        calculateScore();
    }

    public String getName() {
        return name;
    }

    public ArrayList<MainCard> getCards() {
        return hand;
    }

    public int getNumberOfCards() {
        return hand.size();
    }

    public void addCard(MainCard card) {
        hand.add(card);
        calculateScore();
    }

    public void removeCard(MainCard card) {
        hand.remove(card);
        calculateScore();
    }

    private void calculateScore() {
        int count = 0;
        for (MainCard temp: hand) {
             count += temp.getScore();
        }
        this.score = count;
    }

    @Override
    public String toString() {
        return name + ": "+ getNumberOfCards() + " cards, " + score + " scores ";
    }

    public String handToString() {
        StringBuilder
                tHand = new StringBuilder(),
                top = new StringBuilder(),
                midTop1 = new StringBuilder(),
                midTop2 = new StringBuilder(),
                midBot = new StringBuilder(),
                bot = new StringBuilder();

        tHand.append(MainColour.BLACK.getPrintColor());
        for(int i = 1; i <= hand.size() && i <= 15; i++) {
            if(i <= 9) {
                tHand.append("    ").append(i).append("    ");
            }
            else {
                tHand.append("   ").append(i).append("    ");
            }
        }
        tHand.append("\n");
        for(int i = 1; i <= hand.size(); i++)
        {
            MainCard c = hand.get(i-1);
            String[] cs = c.toString().split("\n");
            top.append(cs[0]);
            midTop1.append(cs[1]);
            midTop2.append(cs[2]);
            midBot.append(cs[3]);
            bot.append(cs[4]);
            if(i % 15 == 0 && i != 0)
            {
                bot.append("\n");
                tHand.append(top
                        .append("\n")
                        .append(midTop1.append("\n"))
                        .append(midTop2.append("\n"))
                        .append(midBot.append("\n"))
                        .append(bot.append(MainColour.BLACK.getPrintColor()))
                );
                top = new StringBuilder();
                midTop1 = new StringBuilder();
                midTop2 = new StringBuilder();
                midBot = new StringBuilder();
                bot = new StringBuilder();
                for(int j = 1 + i; j <= hand.size() && j  < 15; j++)
                    tHand.append("   ").append(j).append("    ");
                tHand.append("\n");
            }
        }
        tHand.append(top
                .append("\n")
                .append(midTop1.append("\n"))
                .append(midTop2.append("\n"))
                .append(midBot.append("\n"))
                .append(bot.append("\n"))
        );
        tHand.append(MainColour.BLACK.getPrintColor()).append("\n");
        return tHand.toString();
    }

    public String backHandToString() {
        StringBuilder
                tHand = new StringBuilder(),
                top = new StringBuilder(),
                midTop1 = new StringBuilder(),
                midTop2 = new StringBuilder(),
                midBot = new StringBuilder(),
                bot = new StringBuilder();

        tHand.append(MainColour.BLACK.getPrintColor());
        tHand.append("\n");
        for(int i = 1; i <= hand.size(); i++)
        {
            MainCard mc = hand.get(i-1);
            String[] cs = mc.backToTheString().split("\n");
            top.append(cs[0]);
            midTop1.append(cs[1]);
            midTop2.append(cs[2]);
            midBot.append(cs[3]);
            bot.append(cs[4]);
            if(i % 15 == 0 && i != 0)
            {
                bot.append("\n");
                tHand.append(top
                        .append("\n")
                        .append(midTop1.append("\n"))
                        .append(midTop2.append("\n"))
                        .append(midBot.append("\n"))
                        .append(bot.append(MainColour.BLACK.getPrintColor()))
                );
            }
        }
        tHand.append(top
                .append("\n")
                .append(midTop1.append("\n"))
                .append(midTop2.append("\n"))
                .append(midBot.append("\n"))
                .append(bot.append("\n"))
        );
        tHand.append(MainColour.BLACK.getPrintColor()).append("\n");
        return tHand.toString();
    }
}
