/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package unocardgame;
import java.util.Scanner;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class PlayerControl implements MainControl {
     private MainPlayer player;
    private Scanner scanner;

    public PlayerControl(MainPlayer player, Scanner scanner) {
        this.player = player;
        this.scanner = scanner;
    }

    @Override
    public int putCard(MainCard currentCard) {
        System.out.println(player.getName() + "! Choose a number.(for example 3)");
        int number;
        try {
            number = scanner.nextInt();
        } catch (Exception InputMismatchException) {
            scanner.next();
            number = -1;
        }
        return number - 1;
    }

    @Override
    public char selectTheColor() {
        System.out.println(player.getName() +"! Choose a color between Red and Blue and Green and Yellow.(Enter first character of color for example R)");
        return scanner.next().trim().charAt(0);
    }

    @Override
    public char yesOrNo() {
        System.out.println(player.getName() + "! Do you want to be fined?(Enter Y or N)");
        return scanner.next().trim().charAt(0);
    }
}
