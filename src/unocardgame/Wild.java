/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package unocardgame;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Wild extends ActionCard {
    public Wild(String symbol, MainColour color) {
        super(symbol, color);
        score = 50;
    }

    public void setConfig(char color) {
        if (color == 'R') {
            this.setColor(MainColour.RED);
        }
        if (color == 'B') {
            this.setColor(MainColour.BLUE);
        }
        if (color == 'Y') {
            this.setColor(MainColour.YELLOW);
        }
        if (color == 'G') {
            this.setColor(MainColour.GREEN);
        }
    }
}
