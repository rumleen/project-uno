/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package unocardgame;
import java.util.ArrayList;
import java.util.Collections;
import static java.lang.Integer.max;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class ComputerControl implements MainControl {
    private MainPlayer p;
    private MainState s;

    public ComputerControl(MainPlayer p, MainState s) {
        this.p = p;
        this.s = s;
    }

    @Override
    public int putCard(MainCard currentCard) {
        int number = pcDecidePut(currentCard);
        try
        {
            Thread.sleep(1000);
        }
        catch(InterruptedException ignored){}
        return number;
    }

    @Override
    public char selectTheColor() {
        char color = computerselectedColor();
        try
        {
            Thread.sleep(1000);
        }
        catch(InterruptedException ignored){}
        return color;
    }

    @Override
    public char yesOrNo() {
        System.out.println(p.getName() + "! Do you want to fine?(Y/N)");
        System.out.println('N');
        try
        {
            Thread.sleep(1000);
        }
        catch(InterruptedException ignored){}
        return 'N';
    }
    
    private int pcDecidePut(MainCard currentCard) {
        ArrayList<Integer> wildCards = new ArrayList<>();
        ArrayList<Integer> wildDrawCards = new ArrayList<>();
        ArrayList<Integer> draw2Cards = new ArrayList<>();
        ArrayList<Integer> reverseCards = new ArrayList<>();
        ArrayList<Integer> skipCards = new ArrayList<>();
        ArrayList<Integer> numberCards = new ArrayList<>();
        ArrayList<Integer> colorCards = new ArrayList<>();
        for (int i = 0; i < p.getCards().size(); i++) {
            if (p.getCards().get(i).getColor() == currentCard.getColor()) {
                colorCards.add(i);
            }
            if (p.getCards().get(i) instanceof WildDraw) {
                wildDrawCards.add(i);
            }
            else if(p.getCards().get(i) instanceof Wild) {
                wildCards.add(i);
            }
            if(p.getCards().get(i) instanceof Draw2) {
                draw2Cards.add(i);
            }
            if(p.getCards().get(i) instanceof Reverse) {
                reverseCards.add(i);
            }
            if(p.getCards().get(i) instanceof Skip) {
                skipCards.add(i);
            }
            if( p.getCards().get(i) instanceof Number && p.getCards().get(i).getSymbol().equals(currentCard.getSymbol())) {
                numberCards.add(i);
            }
        }
        Collections.sort(numberCards);
        if (currentCard instanceof WildDraw && s.getWildDrawPenalty() != 1 ) {
            return wildDrawCards.get(0);
        }
        if (currentCard instanceof Draw2 && draw2Cards.size() > 0) {
            return draw2Cards.get(0);
        }
        if(currentCard instanceof Reverse && reverseCards.size() > 0) {
            return reverseCards.get(0);
        }
        if(currentCard instanceof Skip && skipCards.size() > 0) {
            return skipCards.get(0);
        }
        if(colorCards.size() > 0) {
            return colorCards.get(0);
        }
        if (numberCards.size() > 0) {
            return numberCards.get(numberCards.size()-1);
        }
        if(wildCards.size() > 0) {
            return wildCards.get(0);
        }
        if (wildDrawCards.size() > 0) {
            return wildDrawCards.get(0);
        }
        return 1;
    }

    private char computerselectedColor() {
        int countOfR = 0;
        int countOfB = 0;
        int countOfG = 0;
        int countOfY = 0;
        for (MainCard temp : p.getCards()) {
            if (temp.getColor() == MainColour.RED) {
                countOfR++;
            }
            if (temp.getColor() == MainColour.BLUE) {
                countOfB++;
            }
            if (temp.getColor() == MainColour.GREEN) {
                countOfG++;
            }
            if (temp.getColor() == MainColour.YELLOW) {
                countOfY++;
            }
        }
        int max = max( max(countOfR, countOfB), max(countOfG, countOfY));
        if (max == countOfB) {
            return 'B';
        }
        if (max == countOfG) {
            return 'G';
        }
        if (max == countOfY) {
            return 'Y';
        }
        return 'R';
    }
}
